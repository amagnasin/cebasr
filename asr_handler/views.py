import subprocess

from django.core.files.storage import default_storage
from django.http import JsonResponse, HttpResponseBadRequest
from django.shortcuts import render

from .forms import UploadFileForm

def upload_file(request):
    if request.method != 'GET':
        return HttpResponseBadRequest()

    ctx = {
        'page': {
            'title': 'Web Accessible Cebuano Speech Recognizer',
        }
    }
    return render(request, 'upload.html', ctx)

def upload_file_ajax(request):
    if request.method != 'POST':
        return HttpResponseBadRequest()

    form = UploadFileForm(request.POST, request.FILES)
    if form.is_valid():
        file = request.FILES['file']
        print(dir(file))
        print(file.__class__)
        print(file.open)

        path = 'input.wav'
        if (default_storage.exists(path)):
            default_storage.delete(path)
        default_storage.save(path, file)

        result = subprocess.run(['dir'], shell=True, stdout=subprocess.PIPE)
        # configurations:
        #  * shell = True/False
        #  * stdout = subprocess.PIPE/subprocess...
        # result = subprocess.run(['dir'], shell=True, stdout=subprocess.PIPE)
        return JsonResponse({'output': str(result.stdout, 'utf-8')})
    else:
        # TODO: handle errors properly
        return HttpResponseBadRequest()