from django.conf.urls import url

from . import views

urlpatterns = [
	url(r'^upload/$', views.upload_file),
    url(r'^upload/ajax/$', views.upload_file_ajax),
]