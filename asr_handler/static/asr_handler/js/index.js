(function($){
    'use strict';

    // define variables
    let recorder = null;

    // define onclick handlers
    function recordClick() {
        // disable record button, enable stop button
        $('#button-record').attr('disabled', 'disabled');
        $('#button-stop').removeAttr('disabled');

        recorder.record();
    };

    function stopClick() {
        // disable stop button while processing
        $('#button-stop').attr('disabled', 'disabled');

        recorder.stop();
        recorder.exportWAV(function(blob) {
            let data = new FormData();
            data.append('file', blob);
            data.append('csrfmiddlewaretoken', $('[name=csrfmiddlewaretoken]').val());

            $.ajax({
                url: 'ajax/',
                type: 'POST',
                data: data,
                contentType: false,
                processData: false,
                success: function(data) {
                    recorder.clear();
                    $('#output').html(data.output);

                    // enable record button, disable stop button
                    $('#button-record').removeAttr('disabled');
                    $('#button-stop').attr('disabled', 'disabled');
                },
            });
        });
    };

    // initialize
    (function(){
        let audioContext = null;
        try {
            // webkit shim
            window.AudioContext = window.AudioContext || window.webkitAudioContext;
            navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia;
            window.URL = window.URL || window.webkitURL;

            audioContext = new AudioContext;
        } catch (e) {
            alert('No web audio support in this browser!');
            return;
        }

        navigator.mediaDevices.getUserMedia({audio: true}).then(function(stream) {
            recorder = new Recorder(audioContext.createMediaStreamSource(stream));

            // add click listeners            
            $('#button-record').click(recordClick);
            $('#button-stop').click(stopClick);

            // enable record button
            $('#button-record').removeAttr('disabled');
        }, function(e) {
            console.log('No live audio input: ' + e);
        });
    })();
}(jQuery));