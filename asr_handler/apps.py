from django.apps import AppConfig


class AsrHandlerConfig(AppConfig):
    name = 'asr_handler'
