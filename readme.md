# Installation

* install python
* install pip
* install virtualenv and virtualenvwrapper
    * `pip install virtualenv`
    * `pip install virtualenvwrapper`
* create your virtualenv
    * `mkvirtualenv <project name>`
    * `workon <project name>`
* install requirements.txt
    * `pip install -r requirements.txt`
* run server
    * `python manage.py runserver`
* open server http://127.0.0.1:8000/asr_handler/upload/

# Git Cheatsheet

**Pushing changes to remote server**

* `git status` (to check the status of the repository)
* `git add <file...>` or `git add . --all` (to add files to stage for commit)
* `git commit` (to commit the changes)
* `git push` (to upload the changes to remote repository)

**Pulling changes from remote server**

* `git clone https://amagnasin@bitbucket.org/amagnasin/cebasr.git`
* `git pull` (to download the changes from remote repository)

# Error

* https://goo.gl/rStTGz - getUserMedia requires https
    * works on localhost